# Analysis toolkit

Data is downloaded from the InfluxDB database and analysed using the tools provided here both for sensor verification and for quantification of glider performance and handling. Data is typically managed by pandas dataframes and analysed by Python code run through Jupyter notebooks.
