# Instrumentation toolkit

A kit of useful tools for instrumenting and datalogging an aircraft via a distrubed system, particularly applicable to a hang glider.
This comprises of the following sections:

1. A number of instrument modules. These are typically based around an ESP32 microprocessor running micropython with a variety of sensors attached. Code is written and uploaded to the modules via Jupyter notebook
2. An Android mobile phone which provides a WiFi hotspot to which the instrument modules connect. Data is recorded on the phone and displayed in a basic manner to enable verfication of functionality.
3. An InfluxDB time series database to record the data, combined with Grafana for its display
4. A series of Jupyter notebooks for the analysis of the data.
